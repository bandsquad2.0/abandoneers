﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    public float health = 10;
    public GameObject enemyPrefab;
    public Transform transform;
    
    void Update()
    {
        if (health <= 0)
        {
            Destroy(this, 0);
            Instantiate(enemyPrefab);
            Instantiate(enemyPrefab);
        }
    }
    
}
