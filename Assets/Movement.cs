﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    private bool spacePressed;
    private bool shiftPressed;
    private bool wPressed;
    private bool sPressed;
    private bool aPressed;
    private bool dPressed;

    public float forwardForce = 25;
    public float upwardForce = 1;
    public float sideForce = 10;
    public Rigidbody rb;
    public Transform player;
    public Transform ground;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("w"))
        {
            wPressed = true;
        }

        if (Input.GetKeyDown("a"))
        {
            aPressed = true;
        }

        if (Input.GetKeyDown("s"))
        {
            sPressed = true;
        }

        if (Input.GetKeyDown("d"))
        {
            dPressed = true;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            spacePressed = true;
        }

        if (Input.GetKeyDown(KeyCode.RightShift) || Input.GetKeyDown(KeyCode.LeftShift))
        {
            Vector3 scale = transform.localScale;
            Vector3 position = transform.position;
            scale.y = scale.y / 2;
            position.y = scale.y / 2;
            transform.localScale = scale;
            transform.localPosition = position;
        }

        if (Input.GetKeyUp("w"))
        {
            wPressed = false;
        }

        if (Input.GetKeyUp("a"))
        {
            aPressed = false;
        }

        if (Input.GetKeyUp("s"))
        {
            sPressed = false;
        }

        if (Input.GetKeyUp("d"))
        {
            dPressed = false;
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            spacePressed = false;
        }

        if (Input.GetKeyUp(KeyCode.RightShift) || Input.GetKeyUp(KeyCode.LeftShift))
        {
            Vector3 scale = transform.localScale;
            Vector3 position = transform.position;
            scale.y = scale.y * 2;
            position.y = scale.y * 2;
            transform.localScale = scale;
            transform.localPosition = position;
        }
    }

    private void FixedUpdate()
    {
        if (spacePressed)
        {
            rb.AddForce(0, upwardForce, 0);
        }
       
        if (wPressed)
        {
            rb.AddForce(0, 0, forwardForce);
        }

        if (sPressed)
        {
            rb.AddForce(0, 0, -(forwardForce / 2));
        }

        if (aPressed)
        {
            rb.AddForce(-sideForce, 0, 0);
        }

        if (dPressed)
        {
            rb.AddForce(sideForce, 0, 0);
        }
    }
}
