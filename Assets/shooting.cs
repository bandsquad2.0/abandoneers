﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

public class shooting : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform spawner;
    public GameObject enemy;
    public float damage = 10f;
    public float range = 10f;
    public Camera fpsCam;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            shoot();
        }
    }

    private void shoot()
    {
        RaycastHit hit;

        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            Debug.Log(hit.collider.name);
            EnemyStats stats = hit.transform.GetComponent<EnemyStats>();
            if (stats != null)
            {
                stats.health -= 10;
            }
        }
    }
}
